## Simple Weather app

### Features

- React app.
- Use `openweathermap` API to fetch real time weather.
- Use `Fontawesome` icons.
- Typescript.
- Old school React class based components (would be more compact and elegant with functional components 🎯 which I prefer).
- Old school LESS
- Live on: [Netlift](https://weather-23.netlify.app/) and [AWS S3](http://weather-simple.s3-website-us-east-1.amazonaws.com/).
